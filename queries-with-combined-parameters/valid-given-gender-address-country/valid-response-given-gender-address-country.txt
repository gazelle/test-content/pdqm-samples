HTTP/1.1 200 OK
Server: Apache-Coyote/1.1
Set-Cookie: JSESSIONID=wBAZO0g-ugY-AtBEpNeFEfqn; Path=/PatientManager
X-Powered-By: HAPI FHIR 3.3.0 REST Server (FHIR Server; FHIR 3.0.1/DSTU3)
Last-Modified: Mon, 06 Apr 2020 13:03:48 GMT
Content-Type: application/fhir+json;charset=UTF-8
Date: Mon, 06 Apr 2020 13:03:48 GMT
Connection: close

{"resourceType":"Bundle","id":"c6aa102b-f59e-43f2-ad48-f29c04cac2d0","meta":{"lastUpdated":"2020-04-06T15:03:48.523+02:00"},"type":"searchset","total":1,"link":[{"relation":"self","url":"https://gazelle.ihe.net/pdqm-connector/Patient?address-country=AUT&gender=female&given=Lore"}],"entry":[{"fullUrl":"https://gazelle.ihe.net/pdqm-connector/Patient/9096","resource":{"resourceType":"Patient","id":"9096","meta":{"profile":["http://ihe.net/fhir/StructureDefinition/IHE.PDQm.PatientResource"]},"identifier":[{"system":"urn:oid:1.3.6.1.4.1.12559.11.1.4.1.2","value":"DDS-61633"},{"system":"urn:oid:1.3.6.1.4.1.21367.13.20.3000","value":"IHEBLUE-2816"},{"system":"urn:oid:1.3.6.1.4.1.21367.3000.1.6","value":"IHEFACILITY-2816"},{"system":"urn:oid:1.3.6.1.4.1.21367.13.20.2000","value":"IHEGREEN-2816"},{"system":"urn:oid:1.3.6.1.4.1.21367.13.20.1000","value":"IHERED-2816"}],"active":true,"name":[{"use":"official","family":"Pichler","given":["Lore"]}],"gender":"female","birthDate":"1991-08-29","address":[{"use":"home","line":["Brechmühlweg"],"city":"Hallein","postalCode":"5400","country":"AUT"}]}}]}