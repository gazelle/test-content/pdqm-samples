### Build a soapui project with the PDQm samples:

Go to the project and run the soapui-generator.sh script

```
./soapui-generator.sh
```

Indicate the project name and endpoint, 
then the script should create a soapui project with the pdqm samples of the project :

**${PROJECT_NAME}**-soapui_project.xml