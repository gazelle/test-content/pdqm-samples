HTTP/1.1 200 OK
Server: Apache-Coyote/1.1
Set-Cookie: JSESSIONID=ay+nvVu+WM-8RGbN6-I-Dn6L; Path=/PatientManager
X-Powered-By: HAPI FHIR 3.3.0 REST Server (FHIR Server; FHIR 3.0.1/DSTU3)
Last-Modified: Mon, 06 Apr 2020 13:36:08 GMT
Content-Type: application/fhir+json;charset=UTF-8
Date: Mon, 06 Apr 2020 13:36:08 GMT
Connection: close

{"resourceType":"Bundle","id":"3a0acb9b-9d49-4b32-8467-bd4a2f889517","meta":{"lastUpdated":"2020-04-06T15:36:08.289+02:00"},"type":"searchset","total":1,"link":[{"relation":"self","url":"https://gazelle.ihe.net/pdqm-connector/Patient?gender=male&identifier=urn%3Aoid%3A1.3.6.1.4.1.21367.13.20.1000%7CIHERED-1003"}],"entry":[{"fullUrl":"https://gazelle.ihe.net/pdqm-connector/Patient/8340","resource":{"resourceType":"Patient","id":"8340","meta":{"profile":["http://ihe.net/fhir/StructureDefinition/IHE.PDQm.PatientResource"]},"identifier":[{"system":"urn:oid:1.3.6.1.4.1.21367.13.20.1000","value":"IHERED-1003"},{"system":"urn:oid:1.3.6.1.4.1.21367.13.20.2000","value":"IHEGREEN-1003"},{"system":"urn:oid:1.3.6.1.4.1.21367.13.20.3000","value":"IHEBLUE-1003"},{"system":"urn:oid:1.3.6.1.4.1.21367.3000.1.6","value":"IHEFACILITY-1003"}],"active":true,"name":[{"use":"official","family":"Smith","given":["Albert"]}],"gender":"male","birthDate":"1960-12-31","address":[{"use":"home","line":["60 N Saguaro"],"city":"Tucson","state":"AZ","postalCode":"85701","country":"USA"}]}}]}