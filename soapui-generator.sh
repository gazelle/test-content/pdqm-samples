#!/bin/bash
# Written by ycadoret

echo "Project Name :"
read PROJECT_NAME
echo "Project endPoint :"
read endPoint
FILES_COUNT=`ls | wc -l`
RESOURCES=`cat .resources.xml`

exec 3>${PROJECT_NAME}-soapui_project.xml

echo '<?xml version="1.0" encoding="UTF-8"?>
    <con:soapui-project id="7a54c6d9-7840-45fe-aa1b-47352d9cda40" activeEnvironment="Default" name="'${PROJECT_NAME}'" resourceRoot="" soapui-version="5.5.0" abortOnError="false" runType="SEQUENTIAL" xmlns:con="http://eviware.com/soapui/config">
	<con:settings/>
	<con:interface xsi:type="con:RestService" id="d5fe8dd8-e734-4b3a-9d37-5830e9ed45ab" wadlVersion="http://wadl.dev.java.net/2009/02" name="https://gazelle.ihe.net" type="rest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<con:settings/>
		<con:definitionCache type="TEXT" rootPart=""/>
		<con:endpoints>
			<con:endpoint>https://gazelle.ihe.net</con:endpoint>
		</con:endpoints>
        '${RESOURCES}'
    </con:interface>' >&3

for entry in */
do
  testSuiteName=${entry/'/'/''}
  echo '<con:testSuite id="47ded3c9-6d7b-4dea-ae07-bc6d143cec06" name="'${testSuiteName}'"><con:settings/><con:runType>SEQUENTIAL</con:runType>' >&3

  for entry_case in ${testSuiteName}/*/
  do
    testCaseName=${entry_case/'*'/""}
    testCaseName=${testCaseName/$testSuiteName/""}
    testCaseName=${testCaseName/'/'/""}
    testCaseName=${testCaseName/'/'/""}
    testCaseName=${testCaseName/'/'/""}

    if  [[ ${testCaseName} != "" ]]; then
        echo '<con:testCase id="d2589164-399f-49ff-b652-b8058a553928" failOnError="true" failTestCaseOnErrors="true" keepSession="false" maxResults="0" name="'$testCaseName'" searchProperties="true">
               <con:settings/>' >&3

        for entry_step in ${entry}${testCaseName}/*
        do
           if [[ ${entry_step} == *"query"* ]]; then
              URI=`sed -n '1 p' ${entry_step}`
              ACCEPT=`sed -n '/ccept:/p' ${entry_step}`
              ACCEPT=${ACCEPT/"Accept: "/""}
              ACCEPT=${ACCEPT/"accept: "/""}
              URI=${URI/"GET /pdqm-connector/Patient?"/""}
              URI=${URI/"GET https://gazelle.ihe.net/pdqm-connector/Patient?"/""}
              URI=${URI/"GET /pdqm-connector/Patient/"/""}
              URI=${URI/" HTTP/1.1"/""}
              ENTRY=""

              if [[ ${URI} == *"="* ]]; then
                  while [[ ${URI} == *"="* ]]
                   do
                      X=`echo ${URI%%=*}`
                      URI=${URI/"$X="/""}
                      Y=`echo ${URI%%\&*}`
                      URI=${URI/"$Y&"/""}
                      URI=${URI/"$Y"/""}
                      ENTRY=${ENTRY}'<con:entry key="'${X}'" value="'${Y}'"/>'
                  done

                  testStepName=${entry_step/$entry/""}
                  testStepName=${testStepName/$testCaseName/""}
                  testStepName=${testStepName/'/'/""}
                  testStepName=${testStepName/'/'/""}
                  testStepName=${testStepName/'/'/""}
                  testStepName=${testStepName/'.txt'/""}

                  echo '<con:testStep type="restrequest" name="'${testStepName}'" id="3adb265d-3900-48e6-8a1c-58de9274a239">
                          <con:settings/>
                           <con:config service="https://gazelle.ihe.net" resourcePath="/pdqm-connector/Patient"
                                            methodName="search" xsi:type="con:RestRequestStep" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                           <con:restRequest name="Request search" id="3117b3ff-f6cd-4840-98cc-a91219f9edc6" mediaType="application/json">
                                 <con:settings>
                                   <con:setting id="com.eviware.soapui.impl.wsdl.WsdlRequest@request-headers">&lt;xml-fragment/></con:setting>
                                 </con:settings>
                                 <con:endpoint>${#Project#endPoint}</con:endpoint>
                                 <con:request/>
                                 <con:originalUri>https://gazelle.ihe.net/pdqm-connector/Patient</con:originalUri>
                                 <con:assertion type="Valid HTTP Status Codes" id="fda5edb8-e60a-461b-bd03-b91f74fc4412" name="Valid HTTP Status Codes">
                                   <con:configuration><codes>200</codes></con:configuration>
                                 </con:assertion>
                                 <con:credentials>
                                     <con:authType>No Authorization</con:authType>
                                 </con:credentials>
                                <con:jmsConfig JMSDeliveryMode="PERSISTENT"/>
                               <con:jmsPropertyConfig/>
                               <con:parameters>'${ENTRY}'<con:entry key="Accept" value="'${ACCEPT}'"/>
                                 </con:parameters>
                                 </con:restRequest>
                                 </con:config>
                                </con:testStep>' >&3

               elif [[ ${URI} != "" ]]; then

                  testStepName=${entry_step/$entry/""}
                  testStepName=${testStepName/$testCaseName/""}
                  testStepName=${testStepName/'/'/""}
                  testStepName=${testStepName/'/'/""}
                  testStepName=${testStepName/'/'/""}
                  testStepName=${testStepName/'.txt'/""}
                  echo      '<con:testStep type="restrequest" name="'${testStepName}'" id="3adb265d-3900-48e6-8a1c-58de9274a239">
                                        <con:settings/>
                                            <con:config service="https://gazelle.ihe.net" resourcePath="/pdqm-connector/Patient/{id}"
                                                            methodName="retrieve" xsi:type="con:RestRequestStep" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                                            <con:restRequest name="Request retrieve" id="63dc0d37-2803-43f2-932d-b58ce536c34d" mediaType="application/json">
                                          <con:settings/>
                                         <con:endpoint>${#Project#endPoint}</con:endpoint>
                                        <con:request/>
                                        <con:originalUri>https://gazelle.ihe.net/pdqm-connector/Patient/'${URI}'</con:originalUri>
                                        <con:credentials>
                                            <con:authType>No Authorization</con:authType>
                                        </con:credentials>
                                        <con:jmsConfig JMSDeliveryMode="PERSISTENT"/>
                                        <con:jmsPropertyConfig/>
                                     <con:parameters>
                                         <con:entry key="Accept" value="'${ACCEPT}'"/>
                                         <con:entry key="id" value="'${URI}'"/>
                                     </con:parameters>
                                  <con:parameterOrder>
                                        <con:entry>id</con:entry>
                                        <con:entry>Accept</con:entry>
                                  </con:parameterOrder>
                               </con:restRequest>
                            </con:config>
                        </con:testStep>' >&3
               fi
           fi
        done
        echo  "</con:testCase>" >&3
    fi
  done
  echo '<con:properties/></con:testSuite>' >&3
done

echo  '<con:properties>
        <con:property>
        <con:name>endPoint</con:name>
            <con:value>'${endPoint}'</con:value>
        </con:property>
       </con:properties>
     </con:soapui-project>' >&3

`xmlstarlet fo --indent-tab --omit-decl ${PROJECT_NAME}-soapui_project.xml > ${PROJECT_NAME}-soapui_project_.xml`
`mv ${PROJECT_NAME}-soapui_project_.xml ${PROJECT_NAME}-soapui_project.xml`
