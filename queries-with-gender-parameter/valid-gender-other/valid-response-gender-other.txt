HTTP/1.1 200 OK
Date: Mon, 30 Mar 2020 14:01:42 GMT
Server: Apache/2.4.38 (Debian)
Strict-Transport-Security: max-age=63072000; includeSubdomains;
Set-Cookie: JSESSIONID=UDnySP9-GTWz8jg2IZlXMZn7; Path=/PatientManager; Secure
X-Powered-By: HAPI FHIR 3.3.0 REST Server (FHIR Server; FHIR 3.0.1/DSTU3)
Last-Modified: Mon, 30 Mar 2020 14:01:42 GMT
Content-Type: application/fhir+json;charset=UTF-8
Cache-Control: max-age=300
Expires: Mon, 30 Mar 2020 14:06:42 GMT
Connection: close
Transfer-Encoding: chunked

{"resourceType":"Bundle","id":"5fcf4d61-541d-454a-bffb-8629cbd3d125","meta":{"lastUpdated":"2020-03-30T16:01:42.634+02:00"},"type":"searchset","total":2,"link":[{"relation":"self","url":"https://gazelle.ihe.net/pdqm-connector/Patient?gender=other"}],"entry":[{"fullUrl":"https://gazelle.ihe.net/pdqm-connector/Patient/798","resource":{"resourceType":"Patient","id":"798","meta":{"profile":["http://ihe.net/fhir/StructureDefinition/IHE.PDQm.PatientResource"]},"extension":[{"url":"http://hl7.org/fhir/StructureDefinition/patient-mothersMaidenName","valueString":"Lacey-Underall"}],"identifier":[{"system":"urn:oid:1.3.6.1.4.1.12559.11.1.4.1.2","value":"DDS-3a8b3c1d7"}],"active":true,"name":[{"use":"official","family":"Flores","given":["Preston"]}],"gender":"unknown","birthDate":"1983-05-24","address":[{"use":"home","line":["22 Vautrin Avenue"],"city":"Holtsville","state":"NY","postalCode":"11742","country":"USA"}]}},{"fullUrl":"https://gazelle.ihe.net/pdqm-connector/Patient/12412","resource":{"resourceType":"Patient","id":"12412","meta":{"profile":["http://ihe.net/fhir/StructureDefinition/IHE.PDQm.PatientResource"]},"extension":[{"url":"http://hl7.org/fhir/StructureDefinition/patient-mothersMaidenName","valueString":"Canady"}],"identifier":[{"system":"urn:oid:1.3.6.1.4.1.12559.11.1.4.1.2","value":"DDS-38362"}],"active":true,"name":[{"use":"official","family":"Adlere","given":["Rarron"]}],"gender":"unknown","birthDate":"1971-02-17","address":[{"use":"home","line":["L Street North"],"city":"Fort Smith","state":"Arkansas","postalCode":"72904","country":"USA"}]}}]}